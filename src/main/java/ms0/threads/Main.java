package ms0.threads;

 /**
 * Wir erstellen eine Klasse Thread, die erbt. In dieser Klasse
 * initialisieren wir einen String und einen Integer. Die Methode Thread
 * erwartet einen String und einen Namen, die dann direkt die vorher initialisierten
 * Variablen �berschreibt. Mit der Methode start werden die Threads ausgef�hrt.
 * Die Methode "run" wird �berschrieben, damit wir eine Ausgabe erhalten.
 */

class Thread extends java.lang.Thread {
	String Name; int Laufvariable;
	
	Thread(String Name,int Laufvariable){
		this.Name=Name; 
		this.Laufvariable=Laufvariable;
	}
	
	public void run(){
		for(int i=1;i<=Laufvariable;i++){
			System.out.println(Name + " " + i);
			
		}
	}
}

/**
 * In der Hauptklasse werden zwei Objekte der Klasse Threads erstellt (Mit String und Laufvariable).
 * Danach werden sie direkt ausgef�hrt.
 */

public class Main {
    public static void main(String[] args) {
    	Thread hallo = new Thread ("Hallo", 100),
    		   tschuess = new Thread ("Auf Wiedersehen", 100);
    	
    	hallo.start();
    	tschuess.start();
    }
}
