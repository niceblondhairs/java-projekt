package ms0.readfile;

import java.io.*;
import java.util.ArrayList;


public class Main {

    private static void ladeDatei(String datName) {

        ArrayList<String> list = new ArrayList<String>();
        int Laenge = 0;
        File file = new File(datName);
        System.out.println(file.getAbsolutePath());
        if (!file.canRead() || !file.isFile())
            System.exit(0);

        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(datName));
            String zeile = null;
            while ((zeile = in.readLine()) != null) {
                System.out.println("Gelesene Zeile: " + zeile);
                list.add(zeile);
                Laenge++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                }
        }

        for(int j = Laenge-1; j>=0;j--){
            System.out.println(list.get(j));
        }
    }




    public static void main(String[] args) {

        String dateiName = "src/main/resources/ms0/readfile/test.txt";
        ladeDatei(dateiName);
    	/*
        ReadFile readFile = new ReadFile();

        String Datei = "test.txt";
        InputStream Inhalt = readFile.ReadFile(Datei);
        System.out.println(Inhalt.toString());
*/
        /*
        try {
            DataInputStream in = new DataInputStream(
                                             new BufferedInputStream(
                                             new FileInputStream("C:\\Users\\Christian\\IdeaProjects\\7320752\\src\\main\\resources\\ms0\\readfile\\test.txt")));

            System.out.println(in);
            while (in.available()>0 ) {
                System.out.println(in.readUTF());
            }

            in.close();
        } catch(IOException e){
            System.out.print(e);
        } */
        /*
    	Reader f;
    	int c;
    	String s;
    	s = "Das folgende Programm zeigt die Verwendung\r\n";
    	s += "der Klasse StringReader am Beispiel eines\r\n";
    	s += "Programms, das einen Reader konstruiert, der\r\n";
    	s += "den Satz liest, der hier an dieser Stelle steht:\r\n";
    	
      try {
    	  f = new StringReader(s);
    	  while ((c = f.read()) != -1) {
    		  System.out.print((char)c);
    	  }
    	  f.close();
      } catch(IOException e) {
    	  System.out.println("Fehler beim Lesen des Strings");
      } */
      

    }
}
