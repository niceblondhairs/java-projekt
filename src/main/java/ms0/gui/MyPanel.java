package ms0.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * Das Panel bekommt eine Hintergrundfarbe und ein Borderlayout, was die richtige Position der Buttons ermöglicht.
 */
public class MyPanel extends JPanel implements ActionListener {
	MyButton button1 = new MyButton(800,100,"entfernen");
	MyButton button2 = new MyButton(100,500,"rot");
	MyButton button3 = new MyButton(100,500,"schwarz");
	MyButton button4 = new MyButton(800,100,"Bild");
	MyButton buttonTest = new MyButton(100,100,"Test");
	JLabel label = new JLabel();
	
	
	MyPanel() {
    	this.setBackground(Color.green);
    	this.setLayout(new java.awt.BorderLayout());
    	
    	this.add(button1,java.awt.BorderLayout.PAGE_START);
    	this.add(button2,java.awt.BorderLayout.LINE_START);
    	this.add(button3,java.awt.BorderLayout.LINE_END);
    	this.add(button4,java.awt.BorderLayout.PAGE_END);
    	button1.addActionListener(this);
    	button2.addActionListener(this);
    	button3.addActionListener(this);
    	button4.addActionListener(this);
    	
    	this.add(label);
    	
	}
	
	public void actionPerformed(ActionEvent ae) {
		
		 
		if(ae.getSource() == this.button1){
			System.out.println(this);
	        System.out.println("entfernen");
	        
		}
		else if(ae.getSource() == this.button2){
			System.out.println("rot");
			label.setText(("rotes Quadrat"));
        }
        else if (ae.getSource() == this.button3){
        	System.out.println("schwarz");
        }
        else if (ae.getSource() == this.button4){
        	System.out.println("Bild");
        }
	} 
}
