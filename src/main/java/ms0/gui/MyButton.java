package ms0.gui;

import java.awt.Dimension;
import javax.swing.JButton;

/**
 *  Die Buttons werden hier gestaltet mit dem �bergebenen Titel und der Gr��e.
 */

public class MyButton extends JButton {
	
	int height;
	int width;
	String title;
	
	MyButton(int width,int height, String title){
		this.setText(title);
    	this.setPreferredSize(new Dimension(width,height));
	}
	
	
}
